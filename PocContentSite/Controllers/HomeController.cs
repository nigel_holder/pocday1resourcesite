using Microsoft.AspNetCore.Mvc;

namespace PocContentSite.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "In case you need to get in contact.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}